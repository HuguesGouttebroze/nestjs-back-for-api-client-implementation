# Implement the backend of New project, a TODO List
## Next, implement animal's API ReST
## GENERAL Back-End
### Setting up the MongoDB database
- install : 
  - the Mongoose package, 
  - bcrypt
  - NestJS wrapper 
- with the following command:
`npm install --save @nestjs/mongoose @types/bcrypt mongoose bcrypt`

## Architecture NestJS app
- ``main.ts` file get a server feature & listen a the port ???
  - on importe une factory depuis '@nestjs/core'
  - ____ _ _ _ un module depuis le ficher .module
  - on a 1 fnc bootstrap() (ligne 8)
  en async & await (ECMAScript)
  (NestFactory.create(AppModule)) => retourne 1 promesse qu on peut chainer & qui retourne 1 .then
  on pt prefixr la fnc "async" pr dire quelle est asyncrone, puis mettre une fnc 'create" (apres le 'await') qui retourne 1 promesse,
  puis on a le .listen() qui ns retourne aussi 1 promesse, on peut donc y add 1 "await" devant ce qui donne: 
  `await app.listen(3000)` // pr ecouter sur le pour 3000

  * un framework très opinâtre
  - Single Responsabiliyty
    - le controller écoute les requetes entrantes & retourne 1 reponce, 
      ---> SRP: LE CONTROLLEUR NE FAIT k CA & IL LE FAIT BIEN (principe SOLID de SINGLE RESPONSABILITY) 
    - LE SERVICE FOURNIE DES FONCTIONNALITES AU CONTROLEUR
      ---> SRP: LE CONTROLLEUR NE FAIT k CA & IL LE FAIT BIEN (principe SOLID de SINGLE RESPONSABILITY)  
    - le module chargé d encapsuler un controlleur & 1 service


    - on retrouve un typage a base de décorateurs, 
      - un @ + "fnc utilisée"
      ou 
      une simple fnc chargée de décorer qque chose

* 1 controlleur

  - une classe nommé @Controller 
    - un constructeur qui prend dsses parametre:
      - un "acceseur" : pivé, public ...
      - le nom de la variable & son type (c est de l'injection de dependande faite grâce a typescript)
      - on pt faire comprendre au constructeur qu on a besoin d une intence de service en indiquant 1 acceceur private ou public, le nom & le type donné a l 'instance de servise souhaité vouloir fournir via le systeme d injection de dépendance.

      - puis on a un autre decorateur:

        `@Get()`

        chargé d ecouter les requetes entrentes

        on decore une fnc `getHello()` qui retourne 1 String qui est fournit par notre instance de service qui met a disposition une methode `hello()`
          -> & si on va voir ds ce service?
            - on décore (& on importe) `Injectable` (depuis nest/common) 
            - une classe qu on a envie de transformer en service
            - & notre méthode `getHello()` ns retourne le contenu que l on souhaite retourné quand 1 client fera un requete de type `Get`

* 1 module ou on declare 1 controller & 1 service
* on a un decorateur `@Module`
* Pour configurer un module?
  - On decore une classe et on passe un objet de configuration ...

  tel : 
  ```ts
  @Module({ // simple objet avec clé/valeur
    imports: [],
    controllers: [AppController],
    providers: [AppService],
  })
  export class AppModule {}
  ```
### Principe des MODULES ! 
- Pour chaque fonctionnamilé à implémenter, il faut :
  -créer un nouveau module, un nouveau comtroleur ...

## Create a module for the TODO list
- add a controller & a service of course, because the module is encapsuling (?) those two others parts

`nest g mo todos`

* create also the `todos.controler`

```ts
import { Controller } from '@nestjs/common';
 
// "localhost:3000/todos"
@Controller('todos')
export class TodosController {}

```
- on veut ecoyuter le req. de type `Get`, on l importe ...
- on veut faire koi???
  - recup. les todos
  avec la methode `findAll` qui retourne un prommesse et de type : 
  `Promise<any>` 
  et `any`, en attendant de créer une interface...

* create the service `todos.service`
run cmd from Nest CLI & (see all Nest CLI cmd on official documentation [here](https://docs.nestjs.com/cli/usages)) :
`nest generate service todos`
  or
`nest g s todos`

* create some todos, like :
```ts
@Injectable()
export class TodosService {
    todos = [
        {
            id: 1,
            title: "Work on NestJS",
            desc: "Nest, a node.js  opiniatre framework coding  in TypeScript"
        },
        {
            id: 2,
            title: "Coding with RamdaJS",
            desc: "Ramda, a functionnal librairie for JavaScript",
        },
        {
            id: 3,
            title: "Learn async. programmation with RxJS",
            desc: "RxJS, a functionnal reactive librairie for asyncrone programmation",
        }
    ]

    // add findAll() method that get the responsability to return an array []
    findAll(): any[] {
      return this.todos; // as class member, we acced to todos with "this" !
    }
}
```
- Ceci nous donne accès au service depuis le controlleur.

- retour au controleur "todos"
- rappel: on a 1 methode findAll() chargée d ecouter les req. GET, avec le decorateur @Get()
- et on va demender à l'injecteur de ns fournir une intstance de `todos.service`, 
  - donc ds le constructeur, 
  - à l'aide d'1 accésseur `private`, 
  - en `readonly` (comme on sait que ce qu on apporte ne va pas etre modifié, 1 fois initialisé), qu on va nommer `todosService`
```ts
@Controller('todos')
export class TodosController {
                // acceseur + nom de var. qui contiendra l'instance que va ns fournir l'injecteur, ce dernier va ns fournir quoi? il va ns fournir l'instance de TodosService,& qu on pt utiliser ds notre methode decorer avec Get,  
    constructor(private readonly todosService) {

      @Get()
    findAll(): Promise<any[]> {

        // on pt retourner le resultat de l'appel a todosService, puis findAll, ce qui ns permettra d acceder a l url "localhost:3000/todos"

        return this.todosService.findAll();

    }
    }
    
    
}

```
### POST req.
* on utilise le dec. ]Post & @Body, qui permet de récuperer le "body", juste en décorant l'argument, tel "body parser ds express
* on suis le principe de Single Responsability, et on laisse au service la reponsabillié d'ajouter le nouveau todo

  ds todos.service:
  une methode create + un spread operator

### on a des type "any" alors qu on es en TypeScript! on va changer ça!
* ns allons créer une interface(qui est une façon de décrire comment doit se comporter un objet), et notre objet, soit "todos" a 1 id, 1 nom & 1 description ...
* soit `todo.interface.ts`
```ts
export interface Todo {
    id: number;
    title: string;
    done: boolean;
    // on laisse la decription optionnelle (?)
    desc?: string;
}
```
*ù ok, ns pouvons maintenant importer notre type `Todo`

`import { Todo } from 'src/interfaces/todo.interface';`



---

---
<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
