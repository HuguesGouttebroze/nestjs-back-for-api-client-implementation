export interface Todo {
    id: number;
    title: string;
    done: boolean;
    desc?: string;
}