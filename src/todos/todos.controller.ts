import { Body, Controller, Get, Post } from '@nestjs/common';
import { Todo } from 'src/interfaces/todo.interface';
import { TodosService } from './todos.service';

// snippet line 5 allow to listen a request to the following url : 
// "localhost:3000/todos"
@Controller('todos')
export class TodosController {

    constructor(private readonly todosService: TodosService) {}
    /**
     * @return {Promise}
     * @type {Todo[]} // array
     */
    @Get()
    findAll(): Todo[] {
        return this.todosService.findAll();
    }

    @Post()
    createTodo(@Body() newTodo){ // @Body allow to decorate the `createTodo` arguments, like express "body parser"

    }
}
