import { Injectable } from '@nestjs/common';
import { Todo } from 'src/interfaces/todo.interface';

@Injectable()
export class TodosService {
    todos: Todo[] = [
        {
            id: 1,
            title: "Work on NestJS",
            desc: "Nest, a node.js  opiniatre framework coding  in TypeScript",
            done: true
        },
        {
            id: 2,
            title: "Coding with RamdaJS",
            desc: "Ramda, a functionnal librairie for JavaScript",
            done: false
        },
        {
            id: 3,
            title: "Learn async. programmation with RxJS",
            desc: "RxJS, a functionnal reactive librairie for asyncrone programmation",
            done: false
        }
    ];

    findAll(): Todo[] {
        return this.todos;
    }

    create(todo: Todo) {
        this.todos = [...this.todos, todo];
    }
}
